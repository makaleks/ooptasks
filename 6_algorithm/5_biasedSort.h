// "include guard"
#ifndef BIASED_SORT_H_INCLUDED
#define BIASED_SORT_H_INCLUDED 0

#include <vector>
#include <string>
#include <algorithm>

std::vector<std::string>::iterator biasedSort(std::vector<std::string> &v, 
        std::string special = "Untitled")
{
    sort(v.begin(), v.end(), [&special](std::string &s1, std::string &s2) -> bool
            {
                if(!s1.compare(special)) return true;
                else if(!s2.compare(special)) return false;
                // non-zero value is a result of lexicographical sort
                // like for strcmp()
                else return s1.compare(s2) < 0;
            });
    // get first non-special entry iterator
    return find_if_not(v.begin(), v.end(), [&special](std::string &s) -> bool
            {
                return !s.compare(special);
            });
}

#endif
