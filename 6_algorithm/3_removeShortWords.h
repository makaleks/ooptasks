// "include guard"
#ifndef REMOVE_SHORT_WORDS_H_INCLUDED
#define REMOVE_SHORT_WORDS_H_INCLUDED

#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>

std::string removeShortWords(std::ifstream &in, int max_len, std::string finish = "EOF")
{
    if(!in.is_open())
    {
        std::cout << "ERROR: no file opened" << std::endl;
        return std::string();
    }
    std::string str;
    std::vector<std::string> str_store;
    std::cout << "Start reading from input stream. Waiting for EOF or terminating word \"" << finish << '"' << std::endl;
    in >> str;
    while(!in.eof())
    {
        if(str == finish)
        {
            break;
        }
        str_store.push_back(str);
        in >> str;
    }
    auto it = std::remove_if(str_store.begin(), str_store.end(), 
            [&max_len](const std::string &s) -> bool
            {
                return s.length() < max_len;
            });
    str_store.erase(it, str_store.end());
    // join all words to make text
    std::string result;
    for(std::string &s : str_store)
    {
        result.append(s + ' ');
    }
    return result;
}

#endif
