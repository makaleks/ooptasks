// "include guard"
#ifndef IS_PALINDROME_H_INCLUDED
#define IS_PALINDROME_H_INCLUDED

#include <string>
#include <algorithm>
#include <cctype>
// #include <iostream>

bool isPalindrome(std::string str)
{   
    // prepare source string
    auto it_of_end = std::remove_if(str.begin(), str.end(), [](char &c)
            {
                c = std::tolower(c);
                if(std::ispunct(c) || !std::isgraph(c)) return true;
                else return false;
            });
    str.erase(it_of_end, str.end());
    
    /* comment this line to get debug info, uncomment "#include <iostream>"
    std::cout <<std::endl<< (int)str_copy.back() << std::endl;
    std::cout<<str<<'\n';
    for(auto it = str.begin(); it != str.end(); it++)
    {
        std::cout<<(int)*it<<'_';
    }
    std::cout<<std::endl;//*/

    std::string str_copy(str.size(), 0);
    std::reverse_copy(str.begin(), str.end(), str_copy.begin());
    return std::equal(str.begin(), str.end(), str_copy.begin());
}

#endif
