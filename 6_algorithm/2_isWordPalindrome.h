// "include guard"
#ifndef IS_WORD_PALINDROME_H_INCLUDED
#define IS_WORD_PALINDROME_H_INCLUDED 0

#include <string>
#include <stack>
//#include <iostream>

bool isWordPalindrome(std::string str)
{
    // prepare source string
    // temporary flag used to copy "space()" symbol no more than once
    bool _tmp_flag = false;
    std::string::iterator it_of_end = copy_if(str.begin(), str.end(), 
            str.begin(), [&_tmp_flag, &it_of_end](char &c) -> bool
            {
                c = std::tolower(c);
                if(!std::ispunct(c) && std::isalnum(c))
                {
                    it_of_end++;
                    _tmp_flag = false;
                    return true;
                }
                else if(std::isspace(c) && !_tmp_flag)
                {
                    it_of_end++;
                    _tmp_flag = true;
                    c = ' ';
                }
                else return false;
            });
    str.erase(it_of_end, str.end());
    if(str.front() == ' ') str.erase(str.begin());
    if(str.back() == ' ') str.pop_back();
    // get the middle iterator
    int space_num = std::count(str.begin(), str.end(), ' '); 
    if(space_num % 2 == 0)
    {
        return false;
    }
    std::string::iterator it_begin = 
        std::find(str.begin(), str.end(), ' ');
    space_num /= 2;
    int i = 0;
    for(i = 0; i < space_num; i++)
    {
        it_begin = std::find(it_begin + 1, str.end(), ' ');
    }
    // stack to use FILO for blocks of text
    // save blocks
    std::string::iterator it_end = it_begin;
    std::stack<std::string> str_stck;
    while(it_end != str.end())
    {
        it_begin = it_end + 1;
        it_end = std::find(it_begin, str.end(), ' ');
        str_stck.push(std::string(it_begin, it_end));
    }
    // FILO for blocks of text
    it_begin = str.begin();
    while(!str_stck.empty())
    {
        if(!std::equal(
                    str_stck.top().begin(), str_stck.top().end(), it_begin))
        {
            return false;
        }
        it_begin = std::find(it_begin, str.end(), ' ') + 1;
        str_stck.pop();
    }
    return true;
}

#endif
