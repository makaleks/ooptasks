// "include guard"
#ifndef VECTOR_LENGTH_H_INCLUDED
#define VECTOR_LENGTH_H_INCLUDED 0

#include <vector>
#include <numeric>
// uncomment typedef with your type to help compiler show errors
//typedef double T;

// backspace first "/"
//*
template <typename T>
//*/
T vectorLength(std::vector<T> vec)
{
    T result = std::inner_product(vec.begin(), vec.end(), vec.begin(), 0);
    return std::sqrt(result);
}

#endif
