#include "1_isPalindrome.h"
#include "2_isWordPalindrome.h"
#include "3_removeShortWords.h"
#include "4_vectorLength.h"
#include "5_biasedSort.h"
#include "6_criticsPick.h"
#include "7_vectorCount.h"
#include "8_substitutionCipher.h"

#include <iostream>
using namespace std;

void makeBorder()
{
    cout << "\n//////////////////////////////\n" << endl;
}

template<class T> ostream& operator<<(ostream &out, vector<T> &v)
{
    out << '{';
    if(!v.empty())
    {
        out << ' ' << v.front(); 
        for(auto it = v.begin() + 1; it != v.end(); it++)
        {
            out << ", " << *it;
        }
        out << ' ';
    }
    out << '}';
    return out;
}

#include <clocale>

int main(int argc, char **argv)
{
    makeBorder();
    cout << "1) Palindrome string test:" << endl;
    string str1("Was it a car or a cat I saw?"), str2("It was a cat.");
    
    cout << "- Trying (\"" << str1 << "\")...\n  => ";
    cout << (isPalindrome(str1) ? "palindrome" : "not palindrome") << endl;
    cout << "- Trying (\"" << str2 << "\")...\n  => ";
    cout << (isPalindrome(str2) ? "palindrome" : "not palindrome") << endl;
    makeBorder();

    cout << "2) Word palindrome string test:" << endl;
    str1.assign("ba na na ba"), str2.assign("very nice, very nice");

    cout << "- Trying (\"" << str1 << "\")...\n  => ";
    cout << (isWordPalindrome(str1) ? 
            "word palindrome" : "not  word palindrome") << endl;
    cout << "- Trying (\"" << str2 << "\")...\n  => ";
    cout << (isWordPalindrome(str2) ? 
            "word palindrome" : "not word palindrome") << endl;
    makeBorder();

    cout << "3) Remove short words test:" << endl;
    // generate path to file - it depends on where the programm started
    str1.assign(argv[0]);
    const char *dir_separator = "/";
    string::iterator it_end = find_end(str1.begin(), str1.end(),
            dir_separator, dir_separator + 1) + 1;
    str1.erase(it_end, str1.end());
    str1.append("text.txt");
    // activate Russian
    setlocale(LC_ALL, "russian");
    cout << "- Trying (\"" << str1 << "\")...\n";
    ifstream in(str1);
    getline(in, str2);
    in.seekg(0);
    cout << "Before: \"" << str2 << '"' << endl;
    // 1 russian utf-8 (Linux) = 2 bytes
    cout << "After: \"" << removeShortWords(in, 12) << '"' << endl;
    in.close();
    makeBorder();

    cout << "4) Vector length test:" << endl;
    vector<double> vec1 = {3, 4};
    vector<float> vec2 = {0, 2, 4, 5, 6};
    cout << vectorLength(vec1) << " = |" << vec1 << '|' << endl;
    cout << vectorLength(vec2) << " = |" << vec2 << '|' << endl;
    makeBorder();

    cout << "4) Vector length test:" << endl;
    vector<string> vec3 = {"bugi-vugi", "Baracuda", "twinkletoes", "Untitled", "trensalor"};
    vector<string> vec4 = {"question", "Untitled", "eliatrop", "enutrof", "cra", "Untitled", "untitled"};
    // random-access iterators cat be used in arithmetic expressions
    // like ordinary pointers
    cout << vec3 << "\n" << (biasedSort(vec3) - vec3.begin()) << " => ";
    cout << vec3 << endl;
    cout << vec4 << "\n" << (biasedSort(vec4) - vec4.begin()) << " => ";
    cout << vec4 << endl;
    makeBorder();
}
