template <class T>
class SmartPointer {
    T * obj;
    unsigned * ref_count;

    void try_delete()
    {
        if(*ref_count == 0)
        {
            delete obj;
            delete ref_count;
            obj = nullptr;
            ref_count = nullptr;
        }
    }

public:
    T* get()
    {
        return obj;
    }
    SmartPointer(T * object) {
        /* �� ����� ���������� �������� T * obj � ���������� �������
         * ������ � 1. */
        /* set T *obj and ref_count = 1
        */
        ref_count = new unsigned(1);
        obj = object;
    }

    SmartPointer(SmartPointer<T> & sptr) {
        /* ���� ����������� ������� ����� ���������������� ��������� �� ������������
         * ������. ��� ����� ������ ���������� obj � ref_count
         * ������ ��, ��� � sptr. �����,
         * ��������� �� ������� ����� ������ �� obj, ��� �����
         * ��������� ref_count. */
         /*
        Set obj and ref_count as in sptr, increment ref_count
         */
        ref_count = &++*sptr.ref_count;
        obj = sptr.obj;
    }

    ~SmartPointer() {
        /* ���������� ������ �� ������. ���������
         * ref_count. ���� ref_count = 0, ����������� ������ �
         * ���������� ������. */
         /*
         decrement ref_count. if ref_count = 0, free memory
         */
        --*ref_count;
        try_delete();
    }

    SmartPointer<T> & operator=(SmartPointer<T> & sptr) {
        /* ����������� =. ���������� ������ ������� ���������
         * ����� ���������, � ������ - ���������.
         */
        /*
        decrement ref_count, set obj and ref_count, increment ref_count
        */
        --*ref_count;
        try_delete();
        ref_count = sptr.ref_count;
        obj = sptr.obj;
    }
};

#include <iostream>
using namespace std;

class cl1
{
    const char *name;
public:
    int val;
    cl1(int nval, const char *nname) : val(nval), name(nname)
    {
    }
    void out()
    {
        cout << name << ": " << val << endl;
    }
};



int main()
{
    SmartPointer<cl1> ptr1_1(new cl1(100,"class1"));
    SmartPointer<cl1> ptr2_1(new cl1(3,"class2"));

    SmartPointer<cl1> ptr1_2(ptr1_1);
    
    ptr1_1.get()->out();
    ptr1_2.get()->out();
    ptr2_1.get()->out();

    ptr2_1 = ptr1_1;
    cout << endl;

    ptr1_1.get()->out();
    ptr1_2.get()->out();
    ptr2_1.get()->out();

    ++ptr1_1.get()->val;
    cout << endl;

    ptr1_1.get()->out();
    ptr1_2.get()->out();
    ptr2_1.get()->out();//*/
    
    return 0;
}
