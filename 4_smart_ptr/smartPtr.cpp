template <class T>
class SmartPointer {
    T * obj;
    unsigned * ref_count;

    SmartPointer(T * object) {
        /* �� ����� ���������� �������� T * obj � ���������� �������
         * ������ � 1. */
        /* set T *obj and ref_count = 1
        */
    }

    SmartPointer(SmartPointer<T> & sptr) {
        /* ���� ����������� ������� ����� ���������������� ��������� �� ������������
         * ������. ��� ����� ������ ���������� obj � ref_count
         * ������ ��, ��� � sptr. �����,
         * ��������� �� ������� ����� ������ �� obj, ��� �����
         * ��������� ref_count. */
         /*
        Set obj and ref_count as in sptr, increment ref_count
         */
    }

    ~SmartPointer(SmartPointer<T> sptr) {
        /* ���������� ������ �� ������. ���������
         * ref_count. ���� ref_count = 0, ����������� ������ �
         * ���������� ������. */
         /*
         decrement ref_count. if ref_count = 0, free memory
         */
    }

    SmartPointer<T> & operator=(SmartPointer<T> & sptr) {
        /* ����������� =. ���������� ������ ������� ���������
         * ����� ���������, � ������ - ���������.
         */
        /*
        decrement ref_count, set obj and ref_count, increment ref_count
        */
    }
}


