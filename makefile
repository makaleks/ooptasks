CC=g++
CFLAGS=-std=c++11

all: adapter makestring composite smart_ptr stliterators algorithm

adapter:
	$(CC) $(CFLAGS) 1_OK_Adapter/old_adapter.cpp -o 1_OK_Adapter/adapter.out

makestring:
	$(CC) $(CFLAGS) 2_makestring/makestring.cpp -c -o 2_makestring/makestring.o

composite:
	$(CC) $(CFLAGS) 3_composite/OK_CompositeLabCpp.cpp -o 3_composite/OK_CompositeLabCpp.out

smart_ptr:
	$(CC) $(CFLAGS) 4_smart_ptr/OK_smartPtr.cpp -o 4_smart_ptr/OK_smartPtr.out

stliterators:
	$(CC) $(CFLAGS) 5_stliterators/OK_stlIteratorsTask.cpp -o 5_stliterators/OK_stlIteratorsTask.out
algorithm:
	$(CC) $(CFLAGS) 6_algorithm/algorithm.cpp -o 6_algorithm/algorithm.out
