// Purpose.  Adapter design pattern lab
// 
// ������.  ����� ������������ "stack machine", ��� �� ��-����������
// ������ Stack.  �� ������ �� �������� ������������ ���� "legacy"
// ����� ��� ������ ��� ������ ������ Queue, �� ������� "�������������� ����������" 
// ����� ������ � ����� �����������.
// 
// �������.
// o ����� Queue ������ ��������� "�������" ���������� Stack.
// o ������ Queue ��������� ������ ctor, dtor, enque, deque, � isEmpty.  ������ ��
//   ���� ������� ��������� ��������� �� ���������� Queue � ����������
//   Stack.
// o ���� �� ���������� Queue::enque() �� Stack::push(), �� ����������� ���� ���������
//   ���� ��� ��������� ���������� ������ Queue::deque().

#include <iostream>

struct StackStruct {
	int*  array;
	int   sp;
	int   size;
};
typedef StackStruct Stack;

static void initialize(Stack* s, int size) {
	s->array = new int[size];
	s->size = size;
	s->sp = 0;
}
static void cleanUp(Stack* s) {
	delete [] s->array;
}
static int isEmpty(Stack* s) {
	return s->sp == 0 ? 1 : 0;
}
static int isFull(Stack* s) {
	return s->sp == s->size ? 1 : 0;
}
static void push(Stack* s, int item) {
	if (!isFull(s)) s->array[s->sp++] = item;
}
static int pop(Stack* s) {
	if (isEmpty(s)) return 0;
	else            return s->array[--s->sp];
}

class Queue {
	Stack _stack;
public:
	Queue(int size)
	{
		::initialize(&_stack, size);
	}
	void enque(int item)
	{
		::push(&_stack, item);
	}
	int deque()
	{
		Stack temporary;
		::initialize(&temporary, _stack.size);
		while (!::isEmpty(&_stack))
		{
			::push(&temporary, ::pop(&_stack));
		}
		int result = ::pop(&temporary);
		while (!::isEmpty(&temporary))
		{
			::push(&_stack, ::pop(&temporary));
		}
		::cleanUp(&temporary);
		return result;
		/*	
			initialize a local temporary instance of Stack
			loop
			pop() the permanent stack and push() the temporary stack
			pop() the temporary stack and remember this value
			loop
			pop() the temporary stack and push() the permanent stack
			cleanUp the temporary stack
			return the remembered value
		*/
	}
	bool isEmpty() { return ::isEmpty(&_stack); }
};

using namespace std;

//#include <conio.h>

int main() {
	Queue  queue(15);

	for (int i = 0; i < 25; i++) queue.enque(i);
	while (!queue.isEmpty())
		cout << queue.deque() << " ";
	cout << endl;


	/*////////////////////////
	cout << endl << endl << "Press any key to finish programm...";
	_getch();
	cout << endl;//*/

	return 0;
}

// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 
