// Problem.  Approach is now more space efficient, but casting and type
// checking are required to coerce the compiler.  The abstraction needs to
// be improved so that Primitive and Composite can be treated
// transparently in a sibling context, while still maintaining their
// specialization.
//
// Assignment.
// o Create a class Component to serve as a base class.  Primitive and
//   Composite should inherit from Component.
// o Move anything that is (or needs to be) common in both Primitive and
//   Composite up into Component.
// o Currently class Composite is coupled to itself (the argument to add() and
//   the children private data member).  It needs to be coupled only to its
//   abstract base class.
// o You can now remove: NodeType, reportType(), the casting in main() and
//   Composite::traverse(), and the "type checking" in Composite::traverse()

#include <iostream>
#include <vector>
using namespace std;

struct Component
{
    virtual void traverse() = 0;
protected:
    static int output_offset;// possible to use even for multithread, because if not, structure of every message will be broken;
};
int Component::output_offset = 0;

class Primitive : public Component
{
    int value;
public:
    Primitive(int nvalue) : value(nvalue)
    {
    }
    void traverse()
    {
        for(int i = output_offset; i > 0; i--)
            cout << '	';
        cout << value << endl;
    }
};

class Composite : public Component
{
    vector<Component *> children;
public:
    void traverse()
    {
        output_offset++;
        for (auto &el : children)
            el->traverse();
        output_offset--;
    }
    void add(Component *comp)
    {
        children.push_back(comp);
    }
    ~Composite()
    {
        for(Component *ptr : children)
        {
            delete ptr;
        }
    }
};

/*
#include <iostream>
enum NodeType { LEAF, INTERIOR };

using namespace std;

class Primitive {
public:
   Primitive( int val ) : value(val), type(LEAF)  { }
   NodeType reportType()                          { return type; }
   void     traverse()                            { cout << value << " "; }
private:
   int       value;
   NodeType  type;
};

class Composite {
public:
   Composite( int val ) : value(val), type(INTERIOR)  { total = 0; }
   NodeType reportType()                              { return type; }
   void     add( Composite* c )                       { children[total++] = c; }
   void traverse() {
      cout << value << " ";
      for (int i=0; i < total; i++)
         if (children[i]->reportType() == LEAF)
            ((Primitive*) children[i])->traverse();
         else
            children[i]->traverse();
   }
private:
   int         value;
   NodeType    type;
   int         total;
   Composite*  children[99];
};//*/

int main( void ) {
   Composite *store = new Composite, *inside = new Composite, *inside_inside = new Composite;
   inside->add(new Primitive(10));
   inside->add(new Primitive(11));
   inside->add(new Primitive(12));
   
   inside_inside->add(new Primitive(20));
   inside_inside->add(new Primitive(21));

   inside->add(inside_inside);

   store->add(new Primitive(00));
   store->add(new Primitive(01));
   store->add(inside);

   store->traverse();

   delete store;
   
   return 0;
}

// 1 2 5 6 3 7 4
