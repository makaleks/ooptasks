#ifndef MAKESTRING_LIB_INCLUDED
#define MAKESTRING_LIB_INCLUDED
#include <malloc.h>
#include <cstring>
class makestring{
	char *text;
	unsigned short len;
public:
	makestring(){ len = 0, text = 0; }
	makestring(char *source, unsigned short source_len = 0){
		if (!source) { len = 0, text = 0; return; }
		if (source_len) len = source_len;
		else len = strlen(source);
		text = (char*)malloc(len + 1);
		unsigned short i = 0;
		strncpy(text, source, len);
	}
	makestring(const makestring &m){
		len = m.len;
		text = (char*)malloc(len + 1);
		strcpy(text, m.text);
	}
	~makestring(){
		free(text);
	}

	char &operator[](unsigned short i){ if (i < len){ return text[i]; } else{ return text[len - 1]; } }
	char *operator()(unsigned i = 0){ if (i < len){ return text + i; } else{ if (len>0)return text + len - 1; else return text;  } }
	makestring &operator<<(char* str){
		unsigned short i = len - 1;
		unsigned char l = strlen(str);
		for (i; i > 0; i--){
			if (!strncmp(text + i, str,l)){
				len = i;
				text[len] = 0;
				text=(char*)realloc(text, len + 1);
				return *this;
			}
		}
		if (!strncmp(text, str,l)){
			text[i] = 0;
			len = 0;
			text=(char*)realloc(text, 1);
		}
		return *this;
	}

	friend makestring operator+(makestring &m, char *str);
	friend makestring operator+(char *str, makestring &m);
	makestring &operator=(makestring &m){
		text = (char*)realloc(text, m.len + 1);
		strcpy(text, m.text);
		len = m.len;
		return *this;
	}
	makestring &operator=(char *str){
		len = strlen(str);
		free(text);
		text = (char*)malloc(len + 1);
		strcpy(text, str);
		return *this;
	}
	friend makestring operator-(makestring m, char *str);
};
#endif