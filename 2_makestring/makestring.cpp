#include "makestring.h"

	makestring operator+(makestring &m, char *str){
		makestring result;
		result.len = m.len + strlen(str);
		result.text = (char*)malloc(result.len + 1);
		strcpy(result.text, m.text);
		strcat(result.text, str);
		return result;
	}
	makestring operator+(char *str, makestring &m){
		makestring result;
		result.len = m.len + strlen(str);
		result.text = (char*)malloc(result.len + 1);
		strcpy(result.text, m.text);
		strcat(result.text, str);
		return result;
	}
	makestring operator-(makestring m, char *str){
		makestring result;
		for (unsigned short i = 0, len = 0; m.text[i] != 0; i++){
			if (!strcmp(m.text + i, str)){
				len = strlen(str);
				result.len = m.len - len;
				result.text = (char*)malloc(result.len + 1);
				strncpy(result.text, m.text, i);
				while (m.text[i + len] != 0){
					result.text[i] = m.text[i + len];
					i++;
				}
				result.text[i] = m.text[i + len];
				return result;
			}
		}
		return m;
	}