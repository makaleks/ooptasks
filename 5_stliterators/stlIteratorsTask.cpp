#include <iostream>

template <typename T>
class List;

template <class TNode>
class Iterator
{
	/* Helper class to provide pointer like facilities around a node */
	friend class List<typename TNode::value_type>;
	TNode* pNode;

	Iterator(TNode* _pNode) : pNode(_pNode) {}
public:
	//operators
};

template <typename T>
class Node
{
	friend class List<T>;
	friend class Iterator<Node<T> >;

public:
	typedef T value_type;
};

template <typename T>
class List
{
	Node<T>* first;

public:
	typedef Iterator<Node<T> > iterator;
	typedef T 		  value_type;

	void push_back(T data)
	{

	};

	void push_front(T data)
	{

	}

	iterator begin(){}
	iterator end(){}

	bool erase(iterator& _iNode) //True for success, vice versa
	{

	}
};

int main(void)
{
	List<int> list;
	list.push_back(3);
	list.push_back(4);
	list.push_front(2);
	list.push_front(1);

	/*Print all elements*/
	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

	/*Delete second element and reprint*/
	List<int>::iterator tmp = list.begin() + 1;
	list.erase(tmp);

	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

	/*Now delete first node and print again*/
	tmp = list.begin();
	list.erase(tmp);

	for (List<int>::iterator iter = list.begin();
	     iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

	//List object takes care of deletion for us.
	return 0;
}
