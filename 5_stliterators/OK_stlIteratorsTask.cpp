#include <iostream>

//typedef int T;

template <typename T>
class List;
template <typename T>
class Node;

template <typename TNode>
class Iterator
{
	/* Helper class to provide pointer like facilities around a node */
	friend class List<typename TNode::value_type>;
	TNode* _pNode;

	Iterator(TNode* pNode) : _pNode(pNode) {}
public:
	//operators
    Iterator operator++(int x)
    {
        if(_pNode)
        {
            _pNode = _pNode->_next;
        }
        return *this;
    }
    Iterator operator++()
    {
        return (*this)++;
    }
    Iterator operator--(int x)
    {
        if(_pNode)
        {
            _pNode = _pNode->_prev;
        }
        return *this;
    }
    Iterator operator--()
    {
        return --(*this);
    }
    bool operator==(Iterator<TNode> iter)
    {
        return _pNode == nullptr && nullptr == iter._pNode
            || reinterpret_cast<long>(_pNode)*reinterpret_cast<long>(iter._pNode) != 0 &&
                _pNode == iter._pNode;
    }
    bool operator!=(Iterator<TNode> iter)
    {
        return !(*this == iter);
    }
    typename TNode::value_type operator*()
    {
        return _pNode->_val;
    }
};

template <typename T>
class Node
{
	friend class List<T>;
	friend class Iterator<Node<T> >;

public:
	typedef T value_type;
    Node(Node<T> *prev, T val = T()) : _next(nullptr), _prev(prev), _val(val)
    {
        if(prev)
        {
            prev->_next = this;
        }
    }
    ~Node()
    {
        delete _next;
        if(_prev)
        {
            _prev->_next = nullptr;
        }
    }
private:
    value_type _val;
    Node<T> *_next, *_prev;
};

template <typename T>
class List
{
	Node<T> *_first, *_last;

public:
	typedef Iterator<Node<T> > iterator;
	typedef T 		  value_type;

    List() : _first(nullptr), _last(_first)
    {
    }

	void push_back(T data)
	{
       if(_last)
       {
            _last->_next = new Node<T>(_last, data);
            _last = _last->_next;
       }
       else
       {
            _last = _first = new Node<T>(nullptr, data);
       }
	}

	void push_front(T data)
	{
        if(_first)
        {
            _first->_prev = new Node<T>(nullptr, data);
            _first->_prev->_next = _first;
            _first = _first->_prev;
        }
        else
        {
            _last = _first = new Node<T>(nullptr, data);
        }
	}

	iterator begin()
    {
        return iterator(_first);
    }
	iterator end()
    {
        return iterator(nullptr);
    }

	bool erase(iterator& iNode) //True for success, vice versa
	{
        if(iNode._pNode)
        {
            if(iNode._pNode->_prev)
            {
                iNode._pNode->_prev->_next = iNode._pNode->_next;
            }
            else
            {
                _first = iNode._pNode->_next;
            }
        if(iNode._pNode->_next)
            {
                iNode._pNode->_next->_prev = iNode._pNode->_prev;
            }
            else
            {
                _last = iNode._pNode->_prev;
            }
            Node<T> *nextNode = iNode._pNode->_next;
            iNode._pNode->_next = iNode._pNode->_prev = nullptr;
            delete iNode._pNode;
            iNode._pNode = nextNode;
            return true;
        }
        else
        {
            return false;
        }
	}
    ~List()
    {
        delete _first;
    }
};

int main(void)
{
	List<int> list;
	list.push_back(3);
	list.push_back(4);
	list.push_front(2);
	list.push_front(1);

	/*Print all elements*/
	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}
    std::cout << "//////////////////////////////" << std::endl;

	/*Delete second element and reprint*/
	List<int>::iterator tmp = list.begin();
    tmp++;
	list.erase(tmp);

	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

    std::cout << "//////////////////////////////" << std::endl;


	/*Now delete first node and print again*/
	tmp = list.begin();
	list.erase(tmp);

	for (List<int>::iterator iter = list.begin();
	     iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

	//List object takes care of deletion for us.
	return 0;
}

